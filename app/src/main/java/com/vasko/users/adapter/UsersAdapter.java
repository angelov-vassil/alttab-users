package com.vasko.users.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.vasko.users.R;
import com.vasko.users.rest.model.User;

import java.util.ArrayList;
import java.util.List;


public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.Holder> {

    public static class Holder extends RecyclerView.ViewHolder {

        private View      root;
        private ImageView userPhoto;
        private TextView  username;


        public Holder(View itemView) {
            super(itemView);
            root = itemView;
            username = (TextView) root.findViewById(R.id.list_item_user_name);
            userPhoto = (ImageView) root.findViewById(R.id.list_item_user_photo);
        }
    }



    public interface OnItemClickListener {

        void onItemClick(User item);
    }



    private List<User>          mUsers;
    private OnItemClickListener mOnItemClickListener;


    public UsersAdapter(List<User> userList, OnItemClickListener listener) {
        if (userList != null) {
            mUsers = new ArrayList<>(userList);
        } else {
            mUsers = new ArrayList<>();
        }

        mOnItemClickListener = listener;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_user, parent, false);
        return new Holder(view);
    }


    @Override
    public void onBindViewHolder(Holder holder, int position) {
        final User user = mUsers.get(position);
        holder.username.setText(user.getName());
        Picasso.with(holder.root.getContext()).load(user.getPhoto()).into(holder.userPhoto);
        holder.root.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(user);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return mUsers.size();
    }

}
