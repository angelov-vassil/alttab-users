package com.vasko.users.util;


import com.vasko.users.rest.model.Token;


public class TokenManager {

    private Token mToken;


    public void setToken(Token token) {
        mToken = token;
    }


    public String getAccessToken() {
        return mToken != null ? mToken.getAccessToken() : null;
    }

}
