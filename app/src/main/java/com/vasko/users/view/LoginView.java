package com.vasko.users.view;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.vasko.users.R;


public class LoginView extends LinearLayout {

    @Bind(R.id.login_user_email)
    EditText mUserEmail;

    @Bind(R.id.login_user_id)
    EditText mUserFbId;

    @Bind(R.id.login_login_btn)
    Button mLoginBtn;


    public LoginView(Context context) {
        super(context);
        initView();
    }


    public LoginView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }


    public LoginView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }


    private void initView() {
        inflate(getContext(), R.layout.view_login, this);
        ButterKnife.bind(this);
    }


    public void setOnLoginClickListener(View.OnClickListener listener) {
        mLoginBtn.setOnClickListener(listener);
    }


    public String getEmail() {
        return mUserEmail.getText().toString();
    }


    public String getFbId() {
        return mUserFbId.getText().toString();
    }


    public void setEmail(String email) {
        mUserEmail.setText(email);
    }


    public void setFbId(String fbId) {
        mUserFbId.setText(fbId);
    }


    public void setInputEnabled(boolean enabled) {
        mUserEmail.setEnabled(enabled);
        mUserFbId.setEnabled(enabled);
        mLoginBtn.setEnabled(enabled);
    }

}
