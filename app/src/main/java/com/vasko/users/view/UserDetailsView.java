package com.vasko.users.view;


import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.squareup.picasso.Picasso;
import com.vasko.users.R;


public class UserDetailsView extends LinearLayout {

    @Bind(R.id.user_details_photo)
    ImageView mUserPhotoView;

    @Bind(R.id.user_details_toolbar)
    Toolbar mToolbar;

    @Bind(R.id.user_details_email)
    TextView mUserEmailView;


    public UserDetailsView(Context context) {
        super(context);
        initView();
    }


    public UserDetailsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }


    public UserDetailsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }


    private void initView() {
        inflate(getContext(), R.layout.view_user_details, this);
        ButterKnife.bind(this);
    }


    public void setPhoto(String url) {
        Picasso.with(getContext()).load(url).into(mUserPhotoView);
    }


    public void setTitle(String name) {
        mToolbar.setTitle(name);
    }


    public void setEmail(String email) {
        mUserEmailView.setText(email);
    }
}
