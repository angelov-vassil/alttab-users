package com.vasko.users.view;


import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.vasko.users.R;


public class UsersGridView extends LinearLayout {

    @Bind(R.id.users_recycler_view)
    RecyclerView mRecyclerView;


    public UsersGridView(Context context) {
        super(context);
        initView();
    }


    public UsersGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }


    public UsersGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }


    private void initView() {
        inflate(getContext(), R.layout.view_users_grid, this);
        ButterKnife.bind(this);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }


    public void setAdapter(RecyclerView.Adapter adapter) {
        mRecyclerView.setAdapter(adapter);
    }
}
