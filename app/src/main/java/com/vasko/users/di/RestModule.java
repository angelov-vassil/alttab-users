package com.vasko.users.di;


import com.vasko.users.rest.RestClient;
import com.vasko.users.rest.api.UsersApi;
import com.vasko.users.rest.service.LoginRestService;
import com.vasko.users.rest.service.UsersRestService;
import com.vasko.users.util.TokenManager;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;


@Module
public class RestModule {

    @Provides
    @Singleton
    public RestClient provideRestClient() {
        return new RestClient();
    }


    @Provides
    @Singleton
    public UsersApi provideUsersApi(RestClient restClient) {
        return restClient.createService(UsersApi.class);
    }


    @Provides
    @Singleton
    public LoginRestService provideLoginRestService() {
        return new LoginRestService();
    }


    @Provides
    @Singleton
    public UsersRestService provideUsersRestService() {
        return new UsersRestService();
    }


    @Provides
    @Singleton
    public TokenManager provideTokenManager() {
        return new TokenManager();
    }

}
