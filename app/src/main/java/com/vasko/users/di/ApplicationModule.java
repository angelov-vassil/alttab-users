package com.vasko.users.di;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import dagger.Module;
import dagger.Provides;


@Module
public class ApplicationModule {

    private Context mAppContext;


    public ApplicationModule(Context context) {
        mAppContext = context.getApplicationContext();
    }


    @Provides
    public SharedPreferences provideSharedPrefs() {
        return PreferenceManager.getDefaultSharedPreferences(mAppContext);
    }

}
