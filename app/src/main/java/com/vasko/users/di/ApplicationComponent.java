package com.vasko.users.di;


import com.vasko.users.presenter.LoginActivity;
import com.vasko.users.presenter.ShowUsersActivity;
import com.vasko.users.presenter.UserDetailsActivity;
import com.vasko.users.rest.service.LoginRestService;
import com.vasko.users.rest.service.UsersRestService;
import dagger.Component;

import javax.inject.Singleton;


@Component(modules = { ApplicationModule.class, RestModule.class })
@Singleton
public interface ApplicationComponent {

    void inject(ShowUsersActivity showUsersActivity);

    void inject(LoginActivity loginActivity);

    void inject(LoginRestService loginRestService);

    void inject(UsersRestService usersRestService);

    void inject(UserDetailsActivity userDetailsActivity);
}
