package com.vasko.users.rest.service;


import android.support.annotation.NonNull;
import com.vasko.users.application.UsersApplication;
import com.vasko.users.rest.api.UsersApi;
import com.vasko.users.rest.model.User;
import com.vasko.users.rest.model.UserAuthentication;
import com.vasko.users.rest.model.UsersList;
import com.vasko.users.util.TokenManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import javax.inject.Inject;


public class UsersRestService extends RestService {

    @Inject
    TokenManager mTokenManager;

    @Inject
    UsersApi mUsersApi;


    public UsersRestService() {
        UsersApplication.getApplicationComponent().inject(this);
    }


    public void getAllUsers(@NonNull final RequestListener<UsersList> listener) {
        Call<UsersList> usersListCall = mUsersApi.getAllUsers(mTokenManager.getAccessToken());
        usersListCall.enqueue(new Callback<UsersList>() {

            @Override
            public void onResponse(Call<UsersList> call, Response<UsersList> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFail(response.code());
                }
            }


            @Override
            public void onFailure(Call<UsersList> call, Throwable t) {
                listener.onFail(CODE_REQUEST_NOT_MADE);
            }
        });
    }


    public void getUser(long id, final RequestListener<User> listener) {
        Call<User> userCall = mUsersApi.getUser(id);
        userCall.enqueue(new Callback<User>() {

            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFail(response.code());
                }
            }


            @Override
            public void onFailure(Call<User> call, Throwable t) {
                listener.onFail(CODE_REQUEST_NOT_MADE);
            }
        });
    }

}
