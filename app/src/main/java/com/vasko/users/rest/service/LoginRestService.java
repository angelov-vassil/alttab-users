package com.vasko.users.rest.service;


import android.support.annotation.NonNull;
import com.vasko.users.application.UsersApplication;
import com.vasko.users.rest.api.UsersApi;
import com.vasko.users.rest.model.Token;
import com.vasko.users.rest.model.UserAuthentication;
import com.vasko.users.util.TokenManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import javax.inject.Inject;


public class LoginRestService extends RestService {

    @Inject
    UsersApi mUsersApi;

    @Inject
    TokenManager mTokenManager;


    public LoginRestService() {
        UsersApplication.getApplicationComponent().inject(this);
    }


    public void authenticate(@NonNull String fbId, @NonNull String email,
            @NonNull final RequestListener<Token> listener) {
        Call<Token> userAuthenticationCall = mUsersApi.authenticate(fbId, email);
        userAuthenticationCall.enqueue(new Callback<Token>() {

            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                if (response.isSuccessful()) {
                    mTokenManager.setToken(response.body());
                    listener.onSuccess(response.body());
                } else {
                    listener.onFail(response.code());
                }

            }


            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                listener.onFail(CODE_REQUEST_NOT_MADE);
            }
        });
    }


    public void register(@NonNull String fbId, @NonNull String email, @NonNull final RequestListener<Token> listener) {
        final Call<UserAuthentication> registerCall = mUsersApi.add(fbId, email);
        registerCall.enqueue(new Callback<UserAuthentication>() {

            @Override
            public void onResponse(Call<UserAuthentication> call, Response<UserAuthentication> response) {
                if (response.isSuccessful()) {
                    Token token = extractToken(response.body());
                    mTokenManager.setToken(token);
                    listener.onSuccess(token);
                } else {
                    listener.onFail(response.code());
                }
            }


            @Override
            public void onFailure(Call<UserAuthentication> call, Throwable t) {
                listener.onFail(CODE_REQUEST_NOT_MADE);
            }
        });
    }


    private Token extractToken(UserAuthentication userAuthentication) {
        Token token = new Token();
        token.setAccessToken(userAuthentication.getAccessToken());
        token.setExpiresIn(userAuthentication.getExpiresIn());
        token.setTokenType(userAuthentication.getTokenType());
        return token;
    }

}
