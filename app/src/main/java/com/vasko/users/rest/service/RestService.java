package com.vasko.users.rest.service;


import com.vasko.users.rest.model.Token;


public class RestService {

    public static final int CODE_UNAUTHORIZED     = 401;
    public static final int CODE_REQUEST_NOT_MADE = -1;



    public interface RequestListener<T> {

        void onSuccess(T result);

        void onFail(int code);
    }
}
