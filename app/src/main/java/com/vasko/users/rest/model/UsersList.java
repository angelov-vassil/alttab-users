package com.vasko.users.rest.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class UsersList implements Serializable {

    @SerializedName("totalUsers")
    private int totalUsers;

    @SerializedName("users")
    private ArrayList<User> users;


    public int getTotalUsers() {
        return totalUsers;
    }


    public void setTotalUsers(int totalUsers) {
        this.totalUsers = totalUsers;
    }


    public ArrayList<User> getUsers() {
        return users;
    }


    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }


    @Override
    public String toString() {
        return "UsersList{" +
                "totalUsers=" + totalUsers +
                ", users=" + users +
                '}';
    }
}
