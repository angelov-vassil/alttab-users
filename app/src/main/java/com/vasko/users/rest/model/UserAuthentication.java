package com.vasko.users.rest.model;


import com.google.gson.annotations.SerializedName;


/**
 * Created by Vasko on 25-Mar-16.
 */
public class UserAuthentication {

    @SerializedName("id")
    private Long id;

    @SerializedName("name")
    private String name;

    @SerializedName("email")
    private String email;

    @SerializedName("facebook_id")
    private String facebookId;

    @SerializedName("twitter_id")
    private String twitterId;

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("token_type")
    private String tokenType;

    @SerializedName("expires_in")
    private Long expiresIn;


    public Long getId() {
        return id;
    }


    public void setId(Long mId) {
        this.id = mId;
    }


    public String getName() {
        return name;
    }


    public void setName(String mName) {
        this.name = mName;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String mEmail) {
        this.email = mEmail;
    }


    public String getFacebookId() {
        return facebookId;
    }


    public void setFacebookId(String mFacebookId) {
        this.facebookId = mFacebookId;
    }


    public String getTwitterId() {
        return twitterId;
    }


    public void setTwitterId(String mTwitterId) {
        this.twitterId = mTwitterId;
    }


    public String getAccessToken() {
        return accessToken;
    }


    public void setAccessToken(String mAccessToken) {
        this.accessToken = mAccessToken;
    }


    public String getTokenType() {
        return tokenType;
    }


    public void setTokenType(String mTokenType) {
        this.tokenType = mTokenType;
    }


    public Long getExpiresIn() {
        return expiresIn;
    }


    public void setExpiresIn(Long mExpiresIn) {
        this.expiresIn = mExpiresIn;
    }


    @Override
    public String toString() {
        return "UserAuthentication{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", facebookId='" + facebookId + '\'' +
                ", twitterId='" + twitterId + '\'' +
                ", accessToken='" + accessToken + '\'' +
                ", tokenType='" + tokenType + '\'' +
                ", expiresIn=" + expiresIn +
                '}';
    }
}
