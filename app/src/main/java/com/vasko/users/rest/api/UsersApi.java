package com.vasko.users.rest.api;


import com.vasko.users.rest.model.Token;
import com.vasko.users.rest.model.User;
import com.vasko.users.rest.model.UserAuthentication;
import com.vasko.users.rest.model.UsersList;
import retrofit2.Call;
import retrofit2.http.*;


/**
 * Created by Vasko on 25-Mar-16.
 */
public interface UsersApi {

    @FormUrlEncoded
    @POST("/v1/users/add")
    Call<UserAuthentication> add(@Field("facebook_id") String facebookId, @Field("email") String email);

    @FormUrlEncoded
    @POST("/v1/users/auth")
    Call<Token> authenticate(@Field("facebook_id") String facebookId, @Field("email") String email);

    @GET("/v1/users/{id}")
    Call<User> getUser(@Path("id") long id);

    @GET("/v1/users")
    Call<UsersList> getAllUsers(@Header("Authorization") String accessToken);

}
