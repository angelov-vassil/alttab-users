package com.vasko.users.application;


import android.app.Application;
import com.vasko.users.di.ApplicationComponent;
import com.vasko.users.di.ApplicationModule;
import com.vasko.users.di.RestModule;
import com.vasko.users.di.DaggerApplicationComponent;


public class UsersApplication extends Application {

    private static ApplicationComponent sApplicationComponent;


    @Override
    public void onCreate() {
        super.onCreate();
        sApplicationComponent = DaggerApplicationComponent.builder().restModule(new RestModule())
                .applicationModule(new ApplicationModule(this)).build();
    }


    public static ApplicationComponent getApplicationComponent() {
        return sApplicationComponent;
    }

}
