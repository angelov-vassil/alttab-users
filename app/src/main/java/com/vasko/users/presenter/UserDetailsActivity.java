package com.vasko.users.presenter;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.vasko.users.application.UsersApplication;
import com.vasko.users.rest.model.User;
import com.vasko.users.view.UserDetailsView;

import java.io.Serializable;


public class UserDetailsActivity extends AppCompatActivity {

    public static final String INTENT_EXTRA_USER = "user";

    private UserDetailsView mView;
    private User            mUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mView = new UserDetailsView(this);
        setContentView(mView);
        UsersApplication.getApplicationComponent().inject(this);
        resolveIntent();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mUser != null) {
            mView.setPhoto(mUser.getPhoto());
        }
        initView();
    }


    private void initView() {
        if (mUser != null) {
            mView.setPhoto(mUser.getPhoto());
            mView.setTitle(mUser.getName());
            mView.setEmail(mUser.getEmail());
        }
    }


    private void resolveIntent() {
        Intent startIntent = getIntent();
        Serializable potentialUser = startIntent.getSerializableExtra(INTENT_EXTRA_USER);
        if (potentialUser instanceof User) {
            mUser = (User) potentialUser;
        }
    }
}
