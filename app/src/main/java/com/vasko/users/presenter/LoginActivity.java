package com.vasko.users.presenter;


import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.vasko.users.R;
import com.vasko.users.application.UsersApplication;
import com.vasko.users.rest.model.Token;
import com.vasko.users.rest.service.LoginRestService;
import com.vasko.users.rest.service.RestService;
import com.vasko.users.view.LoginView;

import javax.inject.Inject;


public class LoginActivity extends AppCompatActivity {

    private static final String SP_KEY_EMAIL = "sp_key_email";
    private static final String SP_KEY_FB_ID = "sp_key_fb_id";

    @Inject
    LoginRestService mLoginRestService;

    @Inject
    SharedPreferences mSharedPrefs;

    private LoginView mLoginView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UsersApplication.getApplicationComponent().inject(this);
        initView();
        setLastLogin();
    }


    private void initView() {
        mLoginView = new LoginView(this);
        mLoginView.setOnLoginClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                authenticate(mLoginView.getFbId(), mLoginView.getEmail());
            }
        });
        mLoginView.setInputEnabled(true);
        setContentView(mLoginView);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.login_activity_title);
        }
    }


    private void authenticate(final String fbId, final String email) {
        if (!isValidEmail(email) || !isValidId(fbId)) {
            Toast.makeText(LoginActivity.this, R.string.invalid_email_or_id, Toast.LENGTH_SHORT).show();
            return;
        }
        mLoginView.setInputEnabled(false);
        mLoginRestService.authenticate(fbId, email, new RestService.RequestListener<Token>() {

            @Override
            public void onSuccess(Token token) {
                startUsersActivity();
            }


            @Override
            public void onFail(int code) {
                if (code == LoginRestService.CODE_UNAUTHORIZED) {
                    Snackbar.make(mLoginView, R.string.snackbar_text, Snackbar.LENGTH_LONG)
                            .setAction(R.string.snackbar_action, new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    register(fbId, email);
                                }
                            }).show();
                } else {
                    Toast.makeText(LoginActivity.this, R.string.auth_service_unavailable, Toast.LENGTH_SHORT).show();
                }
                mLoginView.setInputEnabled(true);
            }
        });
    }


    private void register(String fbId, String email) {
        if (!isValidEmail(email) || !isValidId(fbId)) {
            Toast.makeText(LoginActivity.this, R.string.invalid_email_or_id, Toast.LENGTH_SHORT).show();
            return;
        }

        mLoginView.setInputEnabled(false);
        mLoginRestService.register(fbId, email, new RestService.RequestListener<Token>() {

            @Override
            public void onSuccess(Token token) {
                startUsersActivity();
            }


            @Override
            public void onFail(int code) {
                Toast.makeText(LoginActivity.this, R.string.existing_username_or_id, Toast.LENGTH_SHORT).show();
                mLoginView.setInputEnabled(true);
            }
        });
    }


    private void startUsersActivity() {
        storeLastLogin();
        startActivity(new Intent(LoginActivity.this, ShowUsersActivity.class));
    }


    private boolean isValidId(String id) {
        return !TextUtils.isEmpty(id);
    }


    private boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    private void storeLastLogin() {
        mSharedPrefs.edit().putString(SP_KEY_EMAIL, mLoginView.getEmail()).putString(SP_KEY_FB_ID, mLoginView.getFbId())
                .apply();
    }


    private void setLastLogin() {
        mLoginView.setEmail(mSharedPrefs.getString(SP_KEY_EMAIL, ""));
        mLoginView.setFbId(mSharedPrefs.getString(SP_KEY_FB_ID, ""));
    }
}


