package com.vasko.users.presenter;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.vasko.users.adapter.UsersAdapter;
import com.vasko.users.application.UsersApplication;
import com.vasko.users.rest.model.User;
import com.vasko.users.rest.model.UsersList;
import com.vasko.users.rest.service.RestService;
import com.vasko.users.rest.service.UsersRestService;
import com.vasko.users.view.UsersGridView;

import javax.inject.Inject;


public class ShowUsersActivity extends AppCompatActivity {

    @Inject
    UsersRestService mUsersRestService;

    private UsersGridView mView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mView = new UsersGridView(this);
        setContentView(mView);
        UsersApplication.getApplicationComponent().inject(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mUsersRestService.getAllUsers(new RestService.RequestListener<UsersList>() {

            @Override
            public void onSuccess(UsersList result) {
                mView.setAdapter(new UsersAdapter(result.getUsers(), new UsersAdapter.OnItemClickListener() {

                    @Override
                    public void onItemClick(User item) {
                        Toast.makeText(ShowUsersActivity.this, item.getName(), Toast.LENGTH_SHORT).show();
                        Intent startDetailsActivity = new Intent(ShowUsersActivity.this, UserDetailsActivity.class);
                        startDetailsActivity.putExtra(UserDetailsActivity.INTENT_EXTRA_USER, item);
                        startActivity(startDetailsActivity);
                    }
                }));
            }


            @Override
            public void onFail(int code) {
                Toast.makeText(ShowUsersActivity.this, "Failed to get users", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
