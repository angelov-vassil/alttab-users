Octa Cimpu  16:40
/*
Main tasks:
 // 1. Integrate Facebook Login (or skip if simulated)
 2. Register yourself as a new user with ANY credentials
 3. List users in the main view
 4. Distribute users in a grid with two columns, make sure it will fit properly on any screen size
// 6. Save users info in local SQLLite database
 7. Tap cell to load user's' full profile
 8. Show user's location on google map
 9. Load your profile. Tap the image to change it (from Camera or Photo Albums)
  10. integrate 
  
Additional Tasks:
 1. Sort users by age and display them in ascending order
 2. Set status bar to white for Main screen only
 3. Use the native Sharing sheet to share the user's description and image on Facebook and the user's name and image on Twitter
 4. Tap "Open in Maps" to open user's location in Map app
 5. Make users list available offline (load from local database)
 6. Make sure the App rotates properly
 7. Upload new profile image using the provided Api
 8. Make the user's' image round on profile page
Notes:
//- for Facebook Login you can use either native or Facebook Sdk
- you can integrate any third party library necessary by importing it in the project or through gradle
- you can use any source of inspiration (previous projects, google, snippets library, etc)
// - do as many tasks as you can in one hour, prioritize the Main tasks
* Find more Api info in README.md and Constants.h
*/
README.md
/*
## Adding users [/users/add]
### Adding [POST]
+ Request (application/x-www-form-urlencoded)
   facebook_id=10205061911258974&email=robert@alttab.co
+ Response 201 (application/json)
  {
  "id": 5,
  "name": "",
  "email": "robert@alttab.co",
  "facebook_id": "10205061911258974",
  "twitter_id": "",
  "access_token": "Kura4UXEVsG2rZo5P6cmBj7Y2OvXJ6nScOWCvfjH",
  "token_type": "Bearer",
  "expires_in": 3600
  }
## Authenticating [/users/auth]
### Authenticating [POST]
+ Request (application/x-www-form-urlencoded)
   facebook_id=10205061911258974&email=robert@alttab.co
   
+ Response 200 (application/json)
  + Body
  {
  "access_token":"BlSlDWuhPLeuq12LB4Fr9f1aZTouMWYk0czVwccx",
  "token_type":"Bearer",
  "expires_in":3600
  }
## User listing [/users]
### List users [GET]
+ Request
  + Headers
  Authorization: Kura4UXEVsG2rZo5P6cmBj7Y2OvXJ6nScOWCvfjH
+ Response 200 (application/json)
  + Body
  {
  "totalUsers":1,
  "users":[
  {
  "id":5,
  "name":"",
  "email":"robert@alttab.co",
  "facebook_id":"10205061911258974",
  "twitter_id":"",
  "photo":null,
  "lat":"0.00000000000000",
  "lon":"0.00000000000000",
  "client_id":"e0d8283eb84f76374f63129754c3ec73",
  "created_at":"2015-08-24 10:54:40",
  "updated_at":"2015-08-24 10:54:40"
  }
  ]
  }
## Get user details [/users/{user_id}]
### Get user [GET]
## Updating user details [/users/{user_id}]
### Update details [POST]
+ Request (multipart/form-data)
  + Headers
  Authorization: Kura4UXEVsG2rZo5P6cmBj7Y2OvXJ6nScOWCvfjH
   
  + Body
   
   email=robert@alttab.co&name=Robert+Bojor&lat=44.212866&lon=28.622957&photo=[PHOTO_BYTES]
+ Response 200 (application/json)
  + Body
   
  {
  "id": 5,
  "name": "Robert Bojor",
  "email": "robert@alttab.co",
  "facebook_id": "10205061911258974",
  "twitter_id": "",
  "photo": "https://ios.atmdev.com/user_uploads/2ccdc3e65e0a58c6e57049cf67eb1792.png",
  "lat": 44.212866,
  "lon": 28.622957,
  "client_id": "e0d8283eb84f76374f63129754c3ec73",
  "created_at": "2015-08-24 10:54:40",
  "updated_at": "2015-08-24 10:57:35"
  }
*/
#define base_url @"https://ios.atmdev.com"
#define add_user_path @"v1/users/add" // POST
#define auth_path @"v1/users/auth" // POST
#define list_users_path @"v1/users" // GET
#define read_user_path @"v1/users/user_id" // GET
#define update_user_path @"v1/users/user_id" // POST